package com.raital.store.discount;
import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
@AutoConfigureMockMvc
public class TestWebApp extends RaitalstorediscountApplicationTests{

	@Autowired
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void testAnyCustomer() throws Exception {
		mockMvc.perform(get("/customer/any_customer?order-amount=200")).andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andDo(print()).andExpect(status().isOk()).andExpect(content().string(containsString("Your Net Paypal Amount is 190.0")));

	}
	
	@Test
	public void testEmployee() throws Exception {
		mockMvc.perform(get("/customer/employee?order-amount=200")).andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andDo(print()).andExpect(status().isOk()).andExpect(content().string(containsString("Your Net Paypal Amount is 140.0")));

	}
	
	@Test
	public void testRegularCustomer() throws Exception {
		mockMvc.perform(get("/customer/regular_customer?order-amount=200")).andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andDo(print()).andExpect(status().isOk()).andExpect(content().string(containsString("Your Net Paypal Amount is 180.0")));

	}

}
