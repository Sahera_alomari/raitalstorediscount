package com.raital.store.discount;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RaitalstorediscountApplication {

	public static void main(String[] args) {
		SpringApplication.run(RaitalstorediscountApplication.class, args);
	}

}
