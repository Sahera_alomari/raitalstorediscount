package com.raital.store.discount;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.raital.store.discount.factory.impl.AnyCustomer;
import com.raital.store.discount.factory.impl.Employee;
import com.raital.store.discount.factory.impl.RegularCustomer;

@Service
public class CustomerServiceImplementation {

	@Autowired
	private Employee employee;
	
	@Autowired
	private RegularCustomer regularCustomer;
	
	@Autowired
	private AnyCustomer anyCustomer;
	
	private static final Map<CustomerType,Customer> handler= new HashMap<CustomerType,Customer>();
	
	@PostConstruct
	private Map<CustomerType,Customer> getObject(){
		handler.put(CustomerType.employee, employee);
		handler.put(CustomerType.any_customer, anyCustomer);
		handler.put(CustomerType.regular_customer, regularCustomer);
		return handler;
	}

	public static Customer createInstance(CustomerType customer) {
		return Optional.ofNullable(handler.get(customer)).orElseThrow(()-> new IllegalArgumentException("Invalid Customer"));
	}
}
