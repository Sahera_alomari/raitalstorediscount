package com.raital.store.discount.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.raital.store.discount.Customer;
import com.raital.store.discount.CustomerServiceImplementation;
import com.raital.store.discount.CustomerType;

@RestController
public class CustomerController {

	@GetMapping("customer/{customer-type}")
	public String getCustomerType(@PathVariable("customer-type") String customerType,@RequestParam(name="order-amount")double order_amount) throws Exception {
		
		Customer customerFactory=CustomerServiceImplementation.createInstance(CustomerType.valueOf(customerType));
		return "Your Net Paypal Amount is "+ customerFactory.getNetPaypalAmount(order_amount);		
	}
}
