package com.raital.store.discount;

public interface Customer {

	Double getNetPaypalAmount(double orderAmount); 
}
