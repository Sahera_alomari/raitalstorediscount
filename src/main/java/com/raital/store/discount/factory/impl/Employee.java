package com.raital.store.discount.factory.impl;

import org.springframework.stereotype.Component;

import com.raital.store.discount.Customer;

@Component
public class Employee implements Customer{

	private double discount=0.3;
	
	@Override
	public Double getNetPaypalAmount(double orderAmount) {
		return orderAmount-(orderAmount*discount);
	}
}
