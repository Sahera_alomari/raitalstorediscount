package com.raital.store.discount.factory.impl;

import org.springframework.stereotype.Component;

import com.raital.store.discount.Customer;

@Component
public class AnyCustomer implements Customer{

	private double discount = 0.05;

	@Override
	public Double getNetPaypalAmount(double orderAmount) {
		if (orderAmount >= 100) {
			int total = (int) orderAmount / 100;
			return orderAmount - (total * 100 * discount);
		}
		return orderAmount;
	}

}
